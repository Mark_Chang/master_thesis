\subsection{Lexical Semantics}
Lexical knowledge can be created by the study of lexical semantics.
Lexical knowledge provides the knowledge of meaning of words and the relation between words.
WordNet \cite{Fellbaum:1998:WN} is a lexical knowledge resource for English.
It groups words (\emph{lemmas}) into sets of \emph{synonyms}, called \emph{synset}, based on their meanings,
and provides semantic relations between these synsets.
There are several types of semantic relations, including:
\begin{itemize}[noitemsep,nolistsep]
   \item \emph{hypernyms} : Y is a \emph{hypernym} of X if every X is a Y (canine is a hypernym of dog)
   \item \emph{hyponyms} : Y is a \emph{hyponym} of X if every Y is a X (dog is a hyponym of canine)
   \item \emph{meronym} : Y is a \emph{meronym} of X if Y is a part of X (tree is a meronym of forest)
   \item \emph{holonym} : Y is a \emph{holonym} of X if X is a part of Y (forest is a holonym of tree)
\end{itemize}
For instance, the word $dog$, is in the synset $Synset(dog)$, and this synset has three lemmas,
including $Lemma(dog)$, $Lemma(domestic\_dog)$ and $Lemma(Canis\_familiaris)$.
The hypernyms of $Synset(dog)$ are $Synset(domestic\_animal)$ and $Synset(canine)$,
and the hyponyms of $Synset(dog)$ are $Synset(puppy)$, $Synset(pooch)$, ..., and $Synset(griffon)$, etc.
These semantic relations are illustrated in \Cref{fig:wordnet1}.
\begin{figure}[!h]
\centering
\pgfdeclarelayer{background}
\pgfdeclarelayer{foreground}
\pgfsetlayers{background,main,foreground}

\tikzstyle{iblock}=[ text centered]

\tikzstyle{lemma} = [iblock, draw=black!40,  %fill=gray!5,
    minimum height=1em, rounded corners]

\tikzstyle{synset} = [iblock, draw=black, thick,%fill=gray!15,
    minimum height=2em, rounded corners]
\tikzstyle{relations}= [draw=black!40,->,dashed,thick]

\begin{tikzpicture}

    \node (d1) [lemma]   {$Lemma(dog)$};
    \node (d2) [lemma,right = 0.2cm of d1]   {$Lemma(domestic\_dog)$};
    \node (d3) [lemma,right = 0.2cm of d2]   {$Lemma(Canis\_familiaris)$};
    \node (d0) [above = 0.2cm of d1]   {$Synset(dog)$};
    \begin{pgfonlayer}{background}
        \path (d0.west |- d0.north)+(-0.2,0.2) node (a) {};
        \path (d0.west |- d0.north)+(+4.5,0.2) node (a2) {};
        \path (d0.west |- d0.north)+(+6.3,0.05) node (a3) {};
        \path (d3.south -| d3.east)+(+0.2,-1.2) node (b) {};
        \path (d3.south -| d3.east)+(-2.5,-1.2) node (b2) {};
        \path (d3.south -| d3.east)+(-6.3,-1.05) node (b3) {};
        \path[synset, %fill=gray!15
             ]
            (a) rectangle (b);
    \end{pgfonlayer}
    \node (dhpr1) [synset, above = 1cm of a2] { $Synset(domestic\_animal)$ };
    \node (dhpr2) [synset, right = 0.2cm of dhpr1] {$Synset(canine)$ };
    \node (dhpo1) [synset, below = 1cm of b2] { $Synset(griffon)$ };
    \node (dhpo2) [ left = 0.2cm of dhpo1] {... };
    \node (dhpo3) [synset, left = 0.2cm of dhpo2] { $Synset(pooch)$};
    \node (dhpo4) [synset, left = 0.2cm of dhpo3] { $Synset(puppy)$};
    \path [relations] (a3) to[out=90,in=270,looseness = 0.3 ] node[midway,left,text=black!70 ]{\emph{hypernym}}(dhpr1) ;
    \path [relations] (a3) to[out=90,in=270,looseness = 0.3 ] node[midway,right,text=black!70]{\emph{hypernym}}(dhpr2) ;
    \path [relations] (b3) to[out=270,in=90,looseness = 0.3 ] node[midway,right,text=black!70]{\emph{hyponym}}(dhpo1) ;
    \path [relations] (b3) to[out=270,in=90,looseness = 0.3 ] node[midway,text=black!70      ]{\emph{hyponym}}(dhpo3) ;
    \path [relations] (b3) to[out=270,in=90,looseness = 0.3 ] node[midway,left,text=black!70 ]{\emph{hyponym}}(dhpo4) ;
    \path [relations] (d1) to[out=270,in=270,looseness = 0.3] node[midway,below,text=black!70]{\emph{synonym}}(d2) ;
    \path [relations] (d2) to[out=270,in=270,looseness = 0.3] node[midway,below,text=black!70]{\emph{synonym}}(d3) ;

\end{tikzpicture}

\caption{Example of Semantic Relations in WordNet} \label{fig:wordnet1}
\end{figure}

VerbNet \cite{Kipper:2007:VBN} is another type of lexical knowledge resource.
It provides the link between syntax and semantics of verbs.
Each VerbNet class contains a set of verbs with a list of thematic roles, such as agent, theme and location, etc.
Several syntactic frames are assigned to each verb class.
Syntactic frames describe all possible surface realizations of the verbs.
For example, the verb class $eat-39.1-1$ has four syntactic frames,
and one of them is $NP\:\:V\:\:NP$, and the corresponding thematic roles are $Agent\:\:V\:\:Patient$.
All the syntactic frames of verb class $eat-39.1-1$ are presented in \Cref{table:verbnet1}.

\begin{table}[!h]
\centering
\begin{tabular}{|l|l|l| } \hline
syntactic frame   &  thematic role & example \\\hline
NP V NP          & Agent V Patient         & Cynthia ate the peach.    \\
NP V             & Agent V                 & Cynthia ate.              \\
NP V PP-Conative & Agent V (at) Patient    & Cynthia ate at the peach. \\
NP V PP.source   & Agent V ((+src)) Source & He ate off of the table.  \\\hline
\end{tabular}
\caption{Example of Syntactic Frame in VerbNet} \label{table:verbnet1}
\end{table}

\subsection{Ontology}
World Knowledge is the knowledge that is independent from language.
For example, the knowledge that \emph{a bicycle has two wheels} is an example of world knowledge.
No matter what kind of language we choose to describe it, the concept of this knowledge remains the same.
However, a problem arises.
How can we formally represent the world knowledge?
The solution is to represent it by ontology,
which formally represents world knowledge as a hierarchy of concepts
and interrelationships between these concepts \cite{Gruber:1993:TAP}.
For example, if we want to describe the world knowledge in \Cref{fig:ontology1},
we can use ontology to conceptualize this knowledge, as illustrated in \Cref{fig:ontology2}.
In this figure, the circular nodes represent the concepts,
and the arrow-headed lines represent the relationships between concepts.
For instance, the concept \emph{Car} is a subclass of the concept \emph{Vehicle},
and its relationship between \emph{Engines} is \emph{has},
and the word \emph{min} represents the cardinality of relationship.
Ontology can also be represented by logical form, such as description logic \cite{Baader:2003:DLH}.
For example, we can represent the ontology in \Cref{fig:ontology2} by description logic, as shown in \eqref{eq:dl1}.
In this formula, the symbol $\sqsubseteq$ means subset $\subseteq$,
the symbol $\sqcap$ means intersection $\cap$,
the symbol $\geq_{3}$ means the cardinality is greater or equal to 3,
and the symbol $=_{2}$ means the cardinality is exactly equal to 2.
\begin{equation}\label{eq:dl1}
\begin{aligned}
& Car \sqsubseteq  Vehicle  \sqcap (\geq_{3} has.Wheel)\sqcap (\geq_{1} has.Engine) \\
& Bicycle \sqsubseteq  Vehicle  \sqcap (=_{2} has.Wheel) \\
\end{aligned}
\end{equation}
\begin{figure}[!h]
\centering
\begin{framed}
\raggedright
A car has more than two wheels. \\
A car has one or more engines.\\
Car is a kind of vehicle. \\
A bicycle has two wheels.\\
Bicycle is a kind of vehicle. \\
\end{framed}
\caption{Example of World Knowledge}\label{fig:ontology1}
\end{figure}
\begin{figure}[!h]
\centering
\tikzstyle {cnode} = [draw,circle,text centered,text width=1.5cm,thick]
\tikzstyle {rel} = [draw=gray,thick,->]
\tikzstyle {rtex} = [midway,above,text=gray]
\begin{tikzpicture}

\node(C1)[cnode]{Vehicle};
\node(C2)[cnode,below left = 2cm of C1]{Car};
\node(C3)[cnode,below right = 2cm of C1]{Bicycle};
\node(C4)[cnode,below = 4cm of C1]{Wheel};
\node(C5)[cnode,left = 3cm of C4]{Engine};

\path [rel] (C2) to[out=90,in=270 ] node[rtex]{subclassOf}(C1) ;
\path [rel] (C3) to[out=90,in=270 ] node[rtex]{subclassOf}(C1) ;
\path [rel] (C2) to[out=270,in=90 ] node[rtex]{has min 3}(C4) ;
\path [rel] (C2) to[out=270,in=90 ] node[rtex]{has min 1}(C5) ;
\path [rel] (C3) to[out=270,in=90 ] node[rtex]{has exactly 2}(C4) ;

\end{tikzpicture}
\caption{Example of Ontology}\label{fig:ontology2}
\end{figure}

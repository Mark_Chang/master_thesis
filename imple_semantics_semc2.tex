\subsection{Semantic Construction for Prepositional Phrase}\label{subsec:semccpp}
In some cases, the sentence may contain Prepositional Phrase (PP).
For example, the sentence in \eqref{eq:semccpp1} contains a PP: 在(at) 家(home),
\begin{equation}\label{eq:semccpp1}
\text{小明(XiaoMing) 在(at) 家(home) 玩(play) 電腦(computer)}
\end{equation}
and the syntax tree of this sentence is in \Cref{fig:semccppfig1}.
\begin{figure}[!h]
\centering
\begin{tikzpicture}[level distance=50pt, sibling distance=0pt]
\Tree
[.\node(N1){S};
 [.\node(N2){\begin{tabular}{c}agent\\NP\end{tabular}};
  [.\node(N3){\begin{tabular}{c}Head\\Nba\end{tabular}};
   \node(N4){小明};
  ]
 ]
 [.\node(N5){\begin{tabular}{c}location\\PP\end{tabular}};
  [.\node(N6){\begin{tabular}{c}Head\\P21\end{tabular}};
   \node(N7){在};
  ]
  [.\node(N8){\begin{tabular}{c}DUMMY\\NP\end{tabular}};
   [.\node(N9){\begin{tabular}{c}Head\\Ncb\end{tabular}};
    \node(N10){家};
   ]
  ]
 ]
 [.\node(N11){\begin{tabular}{c}Head\\VC2\end{tabular}};
  \node(N12){玩};
 ]
 [.\node(N13){\begin{tabular}{c}goal\\NP\end{tabular}};
  [.\node(N14){\begin{tabular}{c}Head\\Nab\end{tabular}};
   \node(N15){電腦};
  ]
 ]
]
\end{tikzpicture}
\caption{Syntax Tree of Sentence with Prepositional Phrase}\label{fig:semccppfig1}
\end{figure}
If we use the previous algorithm to generate logical form from \Cref{fig:semccppfig1},
we will get a redundant atomic formula $\text{在}(n_{1})$, as shown in \eqref{eq:semccpp2}.
\begin{equation}\label{eq:semccpp2}
\begin{aligned}
&\text{小明}(n_{0}) \wedge agent(n_{0},e) \wedge \text{在}(n_{1}) \wedge \text{家}(n_{1})  \\
&\wedge location(n_{1},e) \wedge \text{玩}(e) \wedge \text{電腦}(n_{2}) \wedge goal(n_{2},e) \\
\end{aligned}
\end{equation}
Since the relation between $\text{家}(n_{1})$ and $\text{玩}(e)$ is annotated by the thematic role $location(n_{1},e)$,
the formula $\text{在}(n_{1})$ becomes redundant.
In order to avoid creating the redundant formula, we need to check the tags during the operation Op1.
We should check the tag of the leaf before initializing its semantics.
If the first alphabet of the tag is \emph{P}, it means that this word is a preposition.
Hence, we should not initialize the semantic of this leaf.
We illustrate this process in \Cref{fig:semccppfig2}.
\begin{figure}[!h]
\centering
\tikzstyle{ops}  =[draw=black!50,->,dashed]
\tikzstyle{opstx}=[text=black!50,midway]
\tikzstyle{opt}  =[draw=black,->,dotted,thick]
\tikzstyle{opttx}=[text=black,midway]
\resizebox{\columnwidth}{!}{
\begin{tikzpicture}[level distance=70pt, sibling distance=0pt]
\Tree
 [.\node(N5){\begin{tabular}{c}location\\PP\end{tabular}};
  [.\node(N6){\begin{tabular}{c}Head\\P21\end{tabular}};
   \node(N7){在};
  ]
  [.\node(N8){\begin{tabular}{c}DUMMY\\NP\end{tabular}};
   [.\node(N9){\begin{tabular}{c}Head\\Ncb\end{tabular}};
    \node(N10){家};
   ]
  ]
 ]
\node(W11)[right = 1cm of N10]{$\lambda x.\text{家}(x)$};
\node(W12)[above = 1.5cm of W11]{$\lambda x.\text{家}(x)$};
\node(W13)[above = 1.5cm of W12]{$\lambda x.\text{家}(x)$};
\node(T11)[right = 1cm of N5]{
                               $\lambda P \lambda x \lambda e. location(x,e) \wedge P(x)$
                        };
\node(T12)[right= 1cm of W13]{
                        \begin{tabular}{l}
                                $\lambda P \lambda x \lambda e. location(x,e) \wedge P(x) @ \lambda x.\text{家}(x) @ n_{1} $\\
                                $= \lambda e. location(n_{1},e) \wedge \text{家}(n_{1})$
                        \end{tabular}
                                };
\draw[ops] (N10)   to[out=0  ,in=180 ] node[above,opstx]{\footnotesize{op1}}(W11) ;
\draw[ops] (N5)    to[out=0  ,in=180 ] node[above,opstx]{\footnotesize{op1}}(T11) ;
\draw[ops] (W11)   to[out=90 ,in=270 ] node[left ,opstx]{\footnotesize{op2}}(W12) ;
\draw[ops] (W12)   to[out=90 ,in=270 ] node[left ,opstx]{\footnotesize{op2}}(W13) ;
\draw[ops] (T11)   to[out=270,in=180 ] node[left ,opstx]{\footnotesize{op3}}(T12) ;
\draw[ops] (W13)   to[out=0  ,in=180 ] node[below,opstx]{\footnotesize{op3}}(T12) ;
\draw[opt] (N10)   to[out=30 ,in=300] node[left ,opttx]{\footnotesize{op2}}(N9) ;
\draw[opt] (N9)    to[out=30 ,in=300] node[left ,opttx]{\footnotesize{op2}}(N8) ;
\draw[opt] (N8)    to[out=30,in=300] node[above,opttx]{\footnotesize{op3}}(N5) ;
\node(L11)[above right = 1cm of W11]{};
\node(L12)[right = 2cm of L11]{Steps of semantic construction.};
\node(L21)[below = 0.5cm of L11]{};
\node(L22)[right = 2cm of L21]{Paths of tree traversal from leaves.};
\draw[ops] (L11)  to[out=0,in=180] node[opstx]{\footnotesize{opx}}(L12) ;
\draw[opt] (L21)  to[out=0,in=180] node[opttx]{\footnotesize{opx}}(L22) ;
\end{tikzpicture}
}
\caption{Semantic Construction for Prepositional Phrase}\label{fig:semccppfig2}
\end{figure}
The first step is Op1.
In this step, we should not need to initialize the semantics of the leaf with word 在, because the tag of this word is \emph{P21}.
We only need to initialize the semantic of word 家 as $\lambda x.\text{家}(x)$
and initialize the semantic of thematic role \emph{location} as $\lambda P \lambda x \lambda e. location(x,e) \wedge P(x)$.
The next step is to traverse up the tree from its leaves.
Since there is only one leaf 家 with semantic representation, we only need to traverse up from it.
In addition, the labels of parent and grandparent of this node are HEAD and DUMMY, respectively.
They satisfy the requirement of Op2, so we only need to pass the semantic representation up to its grandparents.
The last step, when we reach the root of the tree, since this role only has a child with semantic representation,
we only need to do Op3.
By using Op3,
we combine the semantics of thematic role with the semantics of its child,
and generate the final result, as shown in \eqref{eq:semccpp3}.
\begin{equation}\label{eq:semccpp3}
\lambda e. location(n_{1},e) \wedge \text{家}(n_{1})
\end{equation}
Comparing to the logical form in \eqref{eq:semccpp2}, this result does not contain the redundant formula $\text{在}(n_{1})$.
It can be further combined with the semantic representations of other nodes in the syntax tree \Cref{fig:semccppfig1},
and generate the final result \eqref{eq:semccpp4}.
\begin{equation}\label{eq:semccpp4}
\begin{aligned}
&\text{小明}(n_{0}) \wedge agent(n_{0},e) \wedge \text{家}(n_{1}) \wedge location(n_{1},e)  \\
&\wedge \text{玩}(e) \wedge \text{電腦}(n_{2}) \wedge goal(n_{2},e)
\end{aligned}
\end{equation}
\subsection{Semantic Construction for Noun Phrase}
In some cases, the noun phrase (NP) is composed by multiple words.
For example, in \eqref{eq:semccnp1}, the NP, 電腦(computer) 遊戲(game), consists of two words.
\begin{equation}\label{eq:semccnp1}
\text{小明(XiaoMing) 玩(play) 電腦(computer) 遊戲(game)}
\end{equation}
We build the syntax tree of this sentence, as illustrated in \Cref{fig:semccnpfig1}.
To construct the semantic representation of NP 電腦(computer) 遊戲(game),
we can use the same algorithm introduced in \Cref{subsec:semParsingAlgo}.
The process of generating the logical form from the subtree of this NP is shown in \Cref{fig:semccnpfig2}.
\begin{figure}[!h]
\centering
\begin{tikzpicture}[level distance=50pt, sibling distance=0pt]
\Tree
[.\node(N1){S};
 [.\node(N2){\begin{tabular}{c}agent\\NP\end{tabular}};
  [.\node(N3){\begin{tabular}{c}Head\\Nba\end{tabular}};
   \node(N4){小明};
  ]
 ]
 [.\node(N5){\begin{tabular}{c}Head\\VC2\end{tabular}};
  \node(N6){玩};
 ]
 [.\node(N7){\begin{tabular}{c}goal\\NP\end{tabular}};
  [.\node(N8){\begin{tabular}{c}property\\Nab\end{tabular}};
   \node(N9){電腦};
  ]
  [.\node(N10){\begin{tabular}{c}Head\\Nac\end{tabular}};
   \node(N11){遊戲};
  ]
 ]
]
\end{tikzpicture}
\caption{Syntax Tree of Sentence with Noun Phrase}\label{fig:semccnpfig1}
\end{figure}
\begin{figure}[!h]
\centering
\tikzstyle{ops}  =[draw=black!50,->,dashed]
\tikzstyle{opstx}=[text=black!50,midway]
\tikzstyle{opt}  =[draw=black,->,dotted,thick]
\tikzstyle{opttx}=[text=black,midway]
\resizebox{\columnwidth}{!}{
\begin{tikzpicture}[level distance=70pt, sibling distance=0pt]
\Tree
 [.\node(N7){\begin{tabular}{c}goal\\NP\end{tabular}};
  [.\node(N8){\begin{tabular}{c}property\\Nab\end{tabular}};
   \node(N9){電腦};
  ]
  [.\node(N10){\begin{tabular}{c}Head\\Nac\end{tabular}};
   \node(N11){遊戲};
  ]
 ]
\node(W11)[left  = 5cm of N9 ]{$\lambda x.\text{電腦}(x)$};
\node(W21)[right = 2cm of N11]{$\lambda x.\text{遊戲}(x)$};
\node(W22)[above = 3.5cm of W21]{$\lambda x.\text{遊戲}(x)$};
\node(T11)[left = 1cm of N8]{
                               $\lambda P \lambda x \lambda e. property(x,e) \wedge P(x)$
                        };
\node(T21)[above right = 1cm and 1cm of N7]{
                               $\lambda P \lambda x \lambda e. goal(x,e) \wedge P(x)$
                        };
\node(T22)[left= 0cm of N7]{
                        \begin{tabular}{l}
                                $\lambda P \lambda x \lambda e. property(x,e) \wedge P(x) @ \lambda x.\text{電腦}(x) @ n_{1} $\\
                                $= \lambda e. property(n_{1},e) \wedge \text{電腦}(n_{1})$
                        \end{tabular}
                                };
\node(T12)[above right= 1.5cm and -9cm of T22]{
                        \begin{tabular}{l}
                                $\lambda P \lambda Q \lambda x . P(x) \wedge Q(x)
                                @\lambda e. property(n_{1},e) \wedge \text{電腦}(n_{1})
                                @\lambda x.\text{遊戲}(x)$ \\
                                $= \lambda x. property(n_{1},x) \wedge \text{電腦}(n_{1}) \wedge \text{遊戲}(x)$
                        \end{tabular}
                                };
\node(T13)[above right= 1.5cm and -12cm of T12]{
                        \begin{tabular}{l}
                                $\lambda P \lambda x \lambda e. goal(x,e) \wedge P(x)
                                @ \lambda x. property(n_{1},x) \wedge \text{電腦}(n_{1}) \wedge \text{遊戲}(x) @ n_{2} $\\
                                $= \lambda e. goal(n_{2},e) \wedge property(n_{1},n_{2}) \wedge \text{電腦}(n_{1}) \wedge \text{遊戲}(n_{2}) $\\
                        \end{tabular}
                                };
\node(Z1)[above= 1cm of T13]{};
\draw[ops] (N9)      to[out=180,in=0   ] node[above,opstx]{\footnotesize{op1}}(W11) ;
\draw[ops] (N11)     to[out=0  ,in=180 ] node[above,opstx]{\footnotesize{op1}}(W21) ;
\draw[ops] (N7)      to[out=90 ,in=270 ] node[above,opstx]{\footnotesize{op1}}(T21) ;
\draw[ops] (N8)      to[out=180,in=0   ] node[above,opstx]{\footnotesize{op1}}(T11) ;
\draw[ops] (W21)     to[out=90 ,in=270 ] node[left ,opstx]{\footnotesize{op2}}(W22) ;
\draw[ops] (W11.180) to[out=130,in=270 ] node[right ,opstx,pos=0.2]{\footnotesize{op3}}(T22.193) ;
\draw[ops] (T11)     to[out=90,in=270 ] node[above,opstx]{\footnotesize{op3}}(T22.193) ;
\draw[ops] (T22)     to[out=90 ,in=270 ] node[above,opstx]{\footnotesize{op4}}(T12) ;
\draw[ops] (W22)     to[out=120,in=270 ] node[left,opstx,pos=0.1]{\footnotesize{op4}}(T12) ;
\draw[ops] (T12)     to[out=90,in=270 ]  node[above,opstx]{\footnotesize{op3}}(T13.320) ;
\draw[ops] (T21.40)  to[out=90,in=270 ] node[above,opstx]{\footnotesize{op3}}(T13.320) ;
\draw[opt] (N9)  to[out=150 ,in=240] node[left ,opttx]{\footnotesize{op3}}(N8) ;
\draw[opt] (N11)  to[out=30 ,in=300] node[right ,opttx]{\footnotesize{op2}}(N10) ;
\draw[opt] (N8)    to[out=150,in=240] node[left,opttx,pos=0.7]{\footnotesize{op4 \& op3}}(N7) ;
\draw[opt] (N10)    to[out=30,in=300] node[right,opttx,pos=0.7]{\footnotesize{op4 \& op3}}(N7) ;
\end{tikzpicture}
}
\caption{Semantic Construction for Noun Phrase}\label{fig:semccnpfig2}
\end{figure}
The first step is Op1.
In this step, we initialize the semantic representations of two words: 電腦 and 遊戲,
and two labels: $goal$ and $property$.
The next step is to traverse up the tree from its leaves.
In the left branch with word 電腦, the parent has label $property$,
so we use Op3 to combine their logical forms, as shown in \eqref{eq:semccnp2}.
\begin{equation} \label{eq:semccnp2}
\begin{aligned}
&\lambda P \lambda x \lambda e. property(x,e) \wedge P(x) @ \lambda x.\text{電腦}(x) @ n_{1} \\
&= \lambda e. property(n_{1},e) \wedge \text{電腦}(n_{1})
\end{aligned}
\end{equation}
In the right branch with word 遊戲, the label of the parent is \emph{HEAD},
so we use Op2 to pass the semantic representation to the parent.
In the next step, we traverse up to the root.
Since the root has two children, and they all have their own semantic representations.
We need to use Op4 to combine their semantic representations first.
We use the logical form $\lambda P \lambda Q \lambda x . P(x) \wedge Q(x)$ to combine the semantics in two branches,
shown in \eqref{eq:semccnp3}.
\begin{equation} \label{eq:semccnp3}
\begin{aligned}
&\lambda P \lambda Q \lambda x . P(x) \wedge Q(x)
@\lambda e. property(n_{1},e) \wedge \text{電腦}(n_{1})
@\lambda x.\text{遊戲}(x) \\
&= \lambda x. property(n_{1},x) \wedge \text{電腦}(n_{1}) \wedge \text{遊戲}(x)
\end{aligned}
\end{equation}
After we combine the semantics in these two branches, we have not finished this procedure yet.
Since the root has the thematic role $goal$,
we need to use Op3 to combine the result of \eqref{eq:semccnp3} with the logical form of the thematic role at root.
This operation is shown in \eqref{eq:semccnp4}, and it produces the final result of logical form of this NP.
\begin{equation} \label{eq:semccnp4}
\begin{aligned}
&\lambda P \lambda x \lambda e. goal(x,e) \wedge P(x)
@ \lambda x. property(n_{1},x) \wedge \text{電腦}(n_{1}) \wedge \text{遊戲}(x) @ n_{2} \\
&= \lambda e. goal(n_{2},e) \wedge property(n_{1},n_{2}) \wedge \text{電腦}(n_{1}) \wedge \text{遊戲}(n_{2})
\end{aligned}
\end{equation}
With this logical form, we can combine it with other nodes in the syntax tree in \Cref{fig:semccnpfig1},
and generate the final result \eqref{eq:semccnp5}.
\begin{equation}\label{eq:semccnp5}
\begin{aligned}
&\text{小明}(n_{0}) \wedge agent(n_{0},e) \wedge \text{玩}(e) \\
&\wedge  goal(n_{2},e)  \wedge property(n_{1},n_{2}) \wedge \text{電腦}(n_{1})\wedge \text{遊戲}(n_{2})
\end{aligned}
\end{equation}
Notice that the formula $property(n_{1},n_{2})$ is not the so-called Neo-Davidsonian semantic representation, 
because the argument $n_{2}$ does not represent an event. 
\subsection{Semantic Construction for DE Phrase}
In Chinese, the word 的(DE), is a functional word that links a noun to an entity to represent the owner of the entity,
or links a noun or adjective to an entity to represent the property of the entity.
For example, in \eqref{eq:semccde1}, the DE word represents that the computer is the father's computer,
and in \eqref{eq:semccde2}, the DE word represents that the computer is the latest computer.\begin{subequations}
\begin{equation}\label{eq:semccde1}
\text{爸爸(father) 的(DE) 電腦(computer)}
\end{equation}
\begin{equation}\label{eq:semccde2}
\text{最新(latest) 的(DE) 電腦(computer)}
\end{equation}
\end{subequations}
When a DE word exists in a sentence,
we need to notice that we should not create its semantic representation as a redundant atomic formula.
For example, the sentence \eqref{eq:semccde3} has the word 的.
\begin{equation}\label{eq:semccde3}
\text{小明(XiaoMing) 玩(play) 爸爸(father) 的(DE) 電腦(computer)}
\end{equation}
The syntax tree of this sentence is shown in \Cref{fig:semccdefig1}.
\begin{figure}[!h]
\centering
\begin{tikzpicture}[level distance=50pt, sibling distance=0pt]
\Tree
[.\node(N1){S};
 [.\node(N2){\begin{tabular}{c}agent\\NP\end{tabular}};
  [.\node(N3){\begin{tabular}{c}Head\\Nba\end{tabular}};
   \node(N4){小明};
  ]
 ]
 [.\node(N5){\begin{tabular}{c}Head\\VC2\end{tabular}};
  \node(N6){玩};
 ]
 [.\node(N7){\begin{tabular}{c}goal\\NP\end{tabular}};
  [.\node(N8){\begin{tabular}{c}property\\N\_的\end{tabular}};
   [.\node(N9){\begin{tabular}{c}head\\Nab\end{tabular}};
    \node(N10){爸爸};
   ]
   [.\node(N11){\begin{tabular}{c}Head\\DE\end{tabular}};
    \node(N12){的};
   ]
  ]
  [.\node(N13){\begin{tabular}{c}Head\\Nab\end{tabular}};
   \node(N14){電腦};
  ]
 ]
]
\end{tikzpicture}
\caption{Syntax Tree of Sentence with DE}\label{fig:semccdefig1}
\end{figure}
In order to build the logical form without redundant atomic formula of the DE word,
we can treat the DE word as the preposition mentioned in \Cref{subsec:semccpp}.
By adopting the same method in \Cref{subsec:semccpp},
we can build the logical form of the DE phrase: 爸爸的電腦(father's computer), as shown in \eqref{eq:semccde4}.
\begin{equation}\label{eq:semccde4}
\begin{aligned}
\lambda e. goal(n_{2},e) \wedge \text{爸爸}(n_{0}) \wedge property(n_{0},n_{2}) \wedge \text{電腦}(n_{2})
\end{aligned}
\end{equation}
Then, with this logical form, we can generate the logical form of syntax tree in \Cref{fig:semccdefig1},
and the final result is shown in \eqref{eq:semccde5}.
\begin{equation}\label{eq:semccde5}
\begin{aligned}
&\text{小明}(n_{1}) \wedge agent(n_{1},e) \wedge \text{玩}(e) \\
&\wedge \text{爸爸}(n_{0}) \wedge property(n_{0},n_{2}) \wedge \text{電腦}(n_{2}) \wedge goal(n_{2},e)
\end{aligned}
\end{equation}
\subsection{Semantic Construction for Embedded Sentence}\label{subsec:semccemb}
In some cases, a sentence can be embedded within another sentence.
This phenomenon is called embedded sentence.
For example, the sentence \eqref{eq:semcces1} contains an embedded sentence: 小明玩電腦(XiaoMing plays computer).
\begin{equation}\label{eq:semcces1}
\text{小華(XiaoHwa) 看(see) 小明(XiaoMing) 玩(play) 電腦(computer)}
\end{equation}
The CKIP Chinese Parser can generate the syntax tree of this example, as illustrated in \Cref{fig:semccesfig1},
and the tag of the embedded sentence is \emph{S}.
%The syntax tree is shown in \Cref{fig:semccesfig1}.
\begin{figure}[!h]
\centering
\begin{tikzpicture}[level distance=50pt, sibling distance=0pt]
\Tree
[.\node(N1){S};
 [.\node(N2){\begin{tabular}{c}agent\\NP\end{tabular}};
  [.\node(N3){\begin{tabular}{c}Head\\Nba\end{tabular}};
   \node(N4){小華};
  ]
 ]
 [.\node(N5){\begin{tabular}{c}Head\\VE2\end{tabular}};
  \node(N6){看};
 ]
 [.\node(N7){\begin{tabular}{c}goal\\S\end{tabular}};
  [.\node(N8){\begin{tabular}{c}agent\\NP\end{tabular}};
   [.\node(N9){\begin{tabular}{c}Head\\Nba\end{tabular}};
    \node(N10){小明};
   ]
  ]
  [.\node(N11){\begin{tabular}{c}Head\\VC2\end{tabular}};
   \node(N12){玩};
  ]
  [.\node(N13){\begin{tabular}{c}goal\\NP\end{tabular}};
   [.\node(N14){\begin{tabular}{c}Head\\Nab\end{tabular}};
    \node(N15){電腦};
   ]
  ]
 ]
]
\end{tikzpicture}
\caption{Syntax Tree of Sentence with Embedded Sentence}\label{fig:semccesfig1}
\end{figure}
We can also build the logical form of this syntax tree by the following procedure.
First, we build the semantic representation of the embedded sentence,
and this process can be done if we use the algorithm in \Cref{fig:ckiptosem1}.
However, in this case, we should not use the operation Op5 to terminate the procedure.
The resulting logical form is shown in \eqref{eq:semcces2}.
In the next step, we use the procedure shown in \Cref{fig:semccesfig2}.
\begin{equation}\label{eq:semcces2}
\lambda e. agent(n_{1},e) \wedge \text{小明}(n_{1}) \wedge  \text{玩}(e) \wedge goal(n_{2},e) \wedge \text{電腦}(n_{2})
\end{equation}
At the beginning, we initialize the semantics of the role $goal$ by Op1,
and then we use Op3 to combine the semantics of the role and the embedded sentence, as shown in \eqref{eq:semcces3}.
\begin{equation}\label{eq:semcces3}
\begin{aligned}
&\lambda P \lambda x \lambda e. goal(x,e) \wedge P(x)\\
&@\lambda e. agent(n_{1},e) \wedge \text{小明}(n_{1}) \wedge  \text{玩}(e) \wedge goal(n_{2},e) \wedge \text{電腦}(n_{2})
@e_{2}\\
&=\lambda e. goal(e_{2},e) \wedge agent(n_{1},e_{2}) \wedge \text{小明}(n_{1})
\wedge  \text{玩}(e_{2}) \wedge goal(n_{2},e_{2}) \wedge \text{電腦}(n_{2})
\end{aligned}
\end{equation}
Notice that we replace the variable $e$ in \eqref{eq:semcces2} by the variable $e_{2}$ during the operation of Op3,
since the verb in the embedded sentence is not the same as the verb in its parent.
After constructing the logical form in \eqref{eq:semcces3}, we can combine it with its parent sentence in \Cref{fig:semccesfig1},
and generate the final result \eqref{eq:semcces4}.
\begin{equation}\label{eq:semcces4}
\begin{aligned}
&\text{小華}(n_{0}) \wedge agent(n_{0},e_{1}) \wedge \text{看}(e_{1}) \wedge \text{小明}(n_{1}) \wedge agent(n_{1},e_{2}) \\
&\wedge \text{玩}(e_{2}) \wedge \text{電腦}(n_{2}) \wedge goal(n_{2},e_{2}) \wedge goal(e_{2},e_{1})
\end{aligned}
\end{equation}
\begin{figure}[!h]
\centering
\tikzstyle{ops}  =[draw=black!50,->,dashed]
\tikzstyle{opstx}=[text=black!50,midway]
\tikzstyle{opt}  =[draw=black,->,dotted,thick]
\tikzstyle{opttx}=[text=black,midway]
\resizebox{\columnwidth}{!}{
\begin{tikzpicture}[level distance=60pt, sibling distance=0pt]
\Tree
 [.\node(N7){\begin{tabular}{c}goal\\S\end{tabular}};
  \edge[roof]; \node(N8){\begin{tabular}{c c c}小明& 玩& 電腦\end{tabular}};
 ]
\node(T1)[right = 1cm of N7 ]{
   $\lambda P \lambda x \lambda e. goal(x,e) \wedge P(x)$
};
\node(T2)[right = 1cm of N8]{
    $\lambda e. agent(n_{1},e) \wedge \text{小明}(n_{1}) \wedge  \text{玩}(e) \wedge goal(n_{2},e) \wedge \text{電腦}(n_{2}) $
};
\node(T3)[above right= 1.5cm and -3cm of N7]{
\begin{tabular}{l}
    $\lambda P \lambda x \lambda e. goal(x,e) \wedge P(x)
    @\lambda e. agent(n_{1},e) \wedge \text{小明}(n_{1}) \wedge  \text{玩}(e) \wedge goal(n_{2},e) \wedge \text{電腦}(n_{2})
    @e_{2}$\\
    $=\lambda e. goal(e_{2},e) \wedge agent(n_{1},e_{2}) \wedge \text{小明}(n_{1})
    \wedge  \text{玩}(e_{2}) \wedge goal(n_{2},e_{2}) \wedge \text{電腦}(n_{2})$\\
\end{tabular}
};
\draw[ops] (N7)      to[out=0,in=180   ] node[above,opstx]{\footnotesize{op1}}(T1) ;
\draw[ops] (N8)      to[out=0,in=180   ] node[above,opstx]{\footnotesize{op1-4}}(T2) ;
\draw[ops] (T1)      to[out=90,in=270   ] node[above,opstx]{\footnotesize{op3}}(T3) ;
\draw[ops] (T2)      to[out=90,in=270   ] node[right,opstx]{\footnotesize{op3}}(T3) ;
\end{tikzpicture}
}
\caption{Semantic Construction for Embedded Sentence}\label{fig:semccesfig2}
\end{figure}

\subsection{Distributional Semantics}
Unlike lexical semantics and ontology that require human labor to build,
distributional semantics can be automatically built by unsupervised machine learning technique.
The basic idea of distributional semantics is based on the hypothesis:
\emph{Words which are similar in meaning occur in similar contexts} \cite{Rubenstein:1965:CCS}.
There is a wide variety of computational models implementing distributional semantics, 
including Latent Semantic Analysis (LSA) \cite{Landauer:1998:LSA}, 
Hyperspace Analogue to Language (HAL) \cite{lund:1995:cogsci}, syntax-based or dependency-based models, and so on.
%is one of the computational models implementing distributional semantics.
In this work, we focus on LSA, which is a statistical technique to measure the semantic similarity between words.
The overall procedure of LSA is demonstrated in \Cref{fig:lsa0}.
This technique converts words into vectors,
and measure the similarity between two words by calculating the cosine similarity between two vectors.
The first step of LSA is to represent the text as a matrix.
In this matrix, each row stands for a unique word and each column stands for a text passage or other context.
Taking an example in \citet{Landauer:1998:LSA},
if we have the data in \Cref{fig:lsa1}, we can compute the matrix as shown in \Cref{fig:lsa2}.
Then, we use singular value decomposition (SVD)
to decompose an $m\times n$ matrix $M$ into a factorization of the form \eqref{eq:lsa1}.
In this equation, $U$ is an $m\times m$ orthogonal matrix, $V$ is an $n\times n$ orthogonal matrix,
and $\Sigma$ is an $m \times n$ diagonal matrix.
SVD can filter out noisy elements and strengthen important values.
Also, it reduces the dimensions of a matrix, making vector-space operation much more efficient.
After applying SVD, each word is represented as a vector with $n$ dimensions.
For every word pair $W_{i}$ and $W_{j}$,
the distance between two term vectors $W_{i}$ and $W_{j}$ can be measured by cosine similarity \eqref{eq:lsa2}.
In this equation, $w_{ik}$ and $w_{jk}$ are the k-th components of vectors $W_{i}$ and $W_{j}$, respectively.
\begin{equation}\label{eq:lsa1}
M = U \times \Sigma \times V^{T}
\end{equation}
\begin{equation}\label{eq:lsa2}
cos(W_{i} , W_{j}) = \frac{W_{i} \cdot W_{j}}{\lvert W_{i}\rvert \lvert W_{j}\rvert}
= \frac{\sum\limits_{k=1}^{n}w_{ik}w_{jk}}{ \sqrt[]{  \sum\limits_{k=1}^{n}w_{ik}^{2} \sum\limits_{k=1}^{n}w_{jk}^{2} } }
\end{equation}


\begin{figure}[!h]
\centering
\tikzstyle{myarrows}=[line width=1mm,draw=gray,fill=gray,-triangle 45,
                      postaction={draw=gray, line width=3mm, shorten >=4mm, -}]
\resizebox{\columnwidth}{!}{
\begin{tikzpicture}

  \node (tc) [] {};
  \node (td) [right = 1.8cm of tc] {};
  \draw [draw=black!50] (tc.north west) rectangle +(2.3cm, -2.7cm);

  \node (tc11) [below=0.0cm of tc] { };
  \node (tc12) [below=0.0cm of td] { };
  \node (tc21) [below=0.3cm of tc] { };
  \node (tc22) [below=0.3cm of td] { };
  \node (tc31) [below=0.6cm of tc] { };
  \node (tc32) [below=0.6cm of td] { };
  \node (tc41) [below=0.9cm of tc] { };
  \node (tc42) [below=0.9cm of td] { };
  \node (tc51) [below=1.2cm of tc] { };
  \node (tc52) [below=1.2cm of td] { };
  \node (tc61) [below=1.5cm of tc] { };
  \node (tc62) [below=1.5cm of td] { };
  \node (lb1) [below right=2.5cm  and -0.5cm of tc] { Training Corpus };

  \path [draw=black] (tc11) -- (tc12);
  \path [draw=black] (tc21) -- (tc22);
  \path [draw=black] (tc31) -- (tc32);
  \path [draw=black] (tc41) -- (tc42);
  \path [draw=black] (tc51) -- (tc52);
  \path [draw=black] (tc61) -- (tc62);

  \draw [myarrows](tc42.east)+(0.3cm,0)--+(1.4cm,0);

    \node (m1) [ below right = -0.6cm and 3.7cm of tc,
 %         matrix of math nodes,left delimiter=(,right delimiter=),text width=1.5mm, text height=2mm,
     ]
     {
     $\begin{bmatrix}
     0 &0 &5 &0 &\cdots \\
     0 &2 &0 &0 &\cdots \\
     4 &0 &3 &0 &\cdots \\
     0 &5 &0 &1 &\cdots \\
     \vdots &\vdots &\vdots &\vdots &\ddots \\
     \end{bmatrix}$
     };
  \node (lb2) [below right=0.1cm  and -3.5cm of m1] {Word-Context Matrix };

  \draw [myarrows](m1.east)+(0.3cm,0)--+(1.7cm,0) node[pos=0.3,above]{SVD};


  \node (vc) [above right = -0.4cm and 2.2cm of m1,anchor=west] {};
  \draw [draw=black!50,thick] (vc.north west) rectangle +(2.7cm, -2.7cm);
  \node (vd) [below = 2.0cm of vc] {};
  \node (vd1) [below right= 0.4cm and 0.3cm of vc] {$W_{1}$};
  \node (vd2) [below right= 0.8cm and 1.5cm of vc] {$W_{2}$};

  \draw[->,draw,thick] (vd) -- (vd1) ;
  \draw[->,draw,thick] (vd) -- (vd2) ;

  \draw [myarrows](vc.east)+(2.8cm,-1.3cm)--+(4.5cm,-1.3cm) node[pos=0.5,above]
                                                                {\begin{tabular}{c}Cosine\\Similarity\end{tabular}};

  \node (lb3) [below right=2.5cm  and -0.5cm of vc] { Vector Space };

  \node (sc) [above right = -1.4cm and 4.6cm of vc,anchor=west] {\begin{tabular}{c}Semantic\\Similarity\end{tabular}};

\end{tikzpicture}
}
\caption{Procedure of Latent Semantic Analysis}\label{fig:lsa0}
\end{figure}


\begin{figure}[!h]
\centering
\begin{framed}
\raggedright
c1: Human machine interface for ABC computer applications\\
c2: A survey of user opinion of computer system response time\\
c3: The EPS user interface management system\\
c4: System and human system engineering testing of EPS\\
c5: Relation of user perceived response time to error measurement\\
m1: The generation of random, binary, ordered trees\\
m2: The intersection graph of paths in trees\\
m3: Graph minors IV: Widths of trees and well-quasi-ordering\\
m4: Graph minors: A survey
\end{framed}
\caption{Example of Text Data}\label{fig:lsa1}
\end{figure}
\begin{figure}[!h]
\centering
\begin{tabular}{c|c c c c c c c c c }
            &   c1  &   c2  &   c3  &   c4  &   c5  &   m1  &   m2  &   m3  &   m4 \\\hline
human       &   1   &   0   &   0   &   1   &   0   &   0   &   0   &   0   &   0  \\
interface   &   1   &   0   &   1   &   0   &   0   &   0   &   0   &   0   &   0  \\
computer    &   1   &   1   &   0   &   0   &   0   &   0   &   0   &   0   &   0  \\
user        &   0   &   1   &   1   &   0   &   1   &   0   &   0   &   0   &   0  \\
system      &   0   &   1   &   1   &   2   &   0   &   0   &   0   &   0   &   0  \\
response    &   0   &   1   &   0   &   0   &   1   &   0   &   0   &   0   &   0  \\
time        &   0   &   1   &   0   &   0   &   1   &   0   &   0   &   0   &   0  \\
EPS         &   0   &   0   &   1   &   1   &   0   &   0   &   0   &   0   &   0  \\
survey      &   0   &   1   &   0   &   0   &   0   &   0   &   0   &   0   &   1  \\
trees       &   0   &   0   &   0   &   0   &   0   &   1   &   1   &   1   &   0  \\
graph       &   0   &   0   &   0   &   0   &   0   &   0   &   1   &   1   &   1  \\
minors      &   0   &   0   &   0   &   0   &   0   &   0   &   0   &   1   &   1  \\
\end{tabular}
\caption{Example of Terms-Contexts Matrix}\label{fig:lsa2}
\end{figure}

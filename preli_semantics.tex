\section{Formal Semantics}
Formal semantics is the study of representing the meaning of natural language by formal logic,
 mainly first-order logic.
For example, sentence \eqref{eq:s1} can be converted into logical form as \eqref{eq:l1}.
\begin{equation} \label{eq:s1}
Brutus\: stabbed\: Ceasar.
\end{equation}
\begin{equation} \label{eq:l1}
stab(Brutus,Ceasar)
\end{equation}
In other words, the sentence \eqref{eq:s1} can be characterized as a predicate-argument structure 
that maps sentences onto logical forms.
In this structure, the verb $stab$ is a predicate,
the subject of the verb $Brutus$ is the first argument,
and object of the verb $Ceasar$ is the second argument.
Although this type of representation can express the relationships between verb, subject and object,
however, it has some drawbacks.
For example, if we add a prepositional phrase (PP) $with \:knife$ into \eqref{eq:s1}, as \eqref{eq:s2}.
If we want to convert \eqref{eq:s2} into semantic representation by this procedure,
it will be transformed into \eqref{eq:l2}.
\begin{equation} \label{eq:s2}
Brutus\: stabbed\: Ceasar\: with \:knife.
\end{equation}
\begin{equation} \label{eq:l2}
stab(Brutus,Ceasar,knife)
\end{equation}
In \eqref{eq:l2}, we can see that the third argument $knife$ is the instrument of the verb $stab$.
However, if we add another PP $in\: the\: agora$ into \eqref{eq:s1}, it will become \eqref{eq:s3}.
\begin{equation} \label{eq:s3}
Brutus\: stabbed\: Ceasar \:in \:the \:agora.
\end{equation}
By this procedure, it will be transformed into logical form \eqref{eq:l3}.
\begin{equation} \label{eq:l3}
stab(Brutus,Ceasar,agora)
\end{equation}
Obviously, the third argument in \eqref{eq:l3}, $agora$, is a location. However, in \eqref{eq:l2},
 the third argument is an instrument, and hence the inconsistency of the type in the third argument arises.
\subsection{Davidsonian Event Semantics} \label{subsec:DavidSem}
Since we do not know that how many PPs can exist in a sentence.
It is impossible to allocate the argument slots of a verb predicate for PPs.
\citet{Davidson:1969:DVS} introduced the notion of \emph{event} as quoted below.
\begin{quotation}
\emph{Adverbial modification is thus seen to be logically on a par with adjectival modification:
what adverbial clauses modify is not verbs but the events that certain verbs introduce.}
\end{quotation}
It means that the phrases that modify a verb actually modify the event introduced by that verb.
With this in mind, we can convert the sentences \eqref{eq:s1}, \eqref{eq:s2} and \eqref{eq:s3}
into logical forms \eqref{eq:le1}, \eqref{eq:le2} and \eqref{eq:le3}, respectively.
\begin{equation} \label{eq:le1}
\exists e.stab(Brutus,Ceasar,e)
\end{equation}
\begin{equation} \label{eq:le2}
\exists e.stab(Brutus,Ceasar,e) \wedge instrument(knife,e)
\end{equation}
\begin{equation} \label{eq:le3}
\exists e.stab(Brutus,Ceasar,e) \wedge location(agora,e)
\end{equation}
In the above equations, the roles of PPs, such as \emph{instrument} and \emph{location}, become predicates.
The variable $e$ is the event of the verb, and it serves as the bridges between the verb and other modifiers such as PP.
By this representation, we can add any number of PPs without changing the counts of argument slots of the verb predicate.
However, another problem arises when some arguments are missing from the verb predicate.
For example, in \eqref{eq:s4}, the object of the verb is missing.
After we convert it into logical form as shown in \eqref{eq:le4}, we do not know how to fill in the slot for the second argument in $stab$.
\begin{equation} \label{eq:s4}
Brutus\: stabbed\: unexpectedly.
\end{equation}
\begin{equation} \label{eq:le4}
\exists e.stab(Brutus,\:,e) \wedge unexpectedly(e)
\end{equation}
\subsection{Neo-Davidsonian Semantics} \label{subsec:NeoDavidSem}
It is possible that the subject, object or other arguments of a verb are absent.
The solution is that we can treat all subjects and objects as modifiers of the verb.
For instance, we can transform the sentence \eqref{eq:s1} into logical form \eqref{eq:lne1}.
Undoubtedly, the sentence \eqref{eq:s4} can be transformed into logical form \eqref{eq:lne4} without any unfilled argument slot.
\begin{equation} \label{eq:lne1}
\exists e.stab(e) \wedge subject(Brutus,e) \wedge object(Ceasar,e)
\end{equation}
\begin{equation} \label{eq:lne4}
\exists e.stab(e) \wedge subject(Brutus,e) \wedge unexpectedly(e)
\end{equation}
With the concept of \emph{thematic role} \cite{Dowty:1989:SR}, which describes the relationship between noun and verb,
we assign the role \emph{agent} to subject of verb $stab$, and the role \emph{patient} to object.
Then, the logical forms \eqref{eq:lne1} and \eqref{eq:lne4} become \eqref{eq:lne11} and \eqref{eq:lne41}, respectively.
\begin{equation} \label{eq:lne11}
\exists e.stab(e) \wedge agent(Brutus,e) \wedge patient(Ceasar,e)
\end{equation}
\begin{equation} \label{eq:lne41}
\exists e.stab(e) \wedge agent(Brutus,e) \wedge unexpectedly(e)
\end{equation}
This kind of representation is called Neo-Davidson Semantics \cite{Parsons:1990:TPS}.

\section{Computational Semantics}\label{subsec:ComputationalSem}
After knowing how to convert a sentence into its semantic representation,
the question is:
\emph{How can we automatically convert sentences into semantic representations by computer program?}
This question is solved by the study of computational semantics.
We can use \emph{Lambda Calculus} to build the semantic representation \cite{BlackburnBos:2005:BBS}.
For example, if we want to convert the sentence \eqref{eq:s1} into logical form \eqref{eq:l1},
the first step is to convert it into syntax tree, as illustrated in \Cref{fig:tree1}.
Then, we can construct the semantic representation by the procedure illustrated in \Cref{fig:treesem1}.
\begin{figure}[!h]
\centering

\begin{tikzpicture}[sibling distance=30pt]
\Tree [.S [.NP [.NNP Brutus ] ]
               [.VP [.VP [.VBD stabbed ] ]
                    [.NP [.NNP Ceasar ] ]
          ]
      ]
\end{tikzpicture}
\caption{Example of Syntax Tree} \label{fig:tree1}
\end{figure}
%In the next step, we define the semantic template for every words in the sentence.
%The semantic template for verb $stab$ is $\lambda y.\lambda x.stab(x,y)$,
%and the semantic templates for proper noun $Brutus$ and $Ceasar$ are $Brutus$ and $Ceasar$, respectively.
%Then, we combine these semantic templates in accordance with the structure of the syntax tree \Cref{fig:tree1}.
%At the beginning, then, we combine the semantic by function application.
\begin{figure}[!h]
\centering
\begin{tikzpicture}[sibling distance=30pt]
\Tree [.\node(S1){S}; [.NP [.NNP \node(NNP1){Brutus}; ] ]
               [.\node(VP1){VP}; [.VP [.VBD \node(VBD1){stabbed}; ] ]
                    [.NP [.NNP \node(NNP2){Ceasar}; ] ]
          ]
      ]
\node (SEMV1) [below = 1cm of VBD1]{$\lambda y. \lambda x. stab(x,y)$};
\node (SEMN1) [below = 1cm of NNP1]{$Brutus$};
\node (SEMN2) [below = 1cm of NNP2]{$Ceasar$};
\node (SEM1) [base right=of VP1]{
                        \begin{tabular}{c}
                            $\lambda y. \lambda x. stab(x,y) @ (Ceasar)$ \\
                            $=\lambda x. stab(x,Ceasar)$
                        \end{tabular}
                        };

\node (SEM2) [above of=S1]{
                        \begin{tabular}{c}
                            $\lambda x. stab(x,Ceasar) @ (Brutus)$ \\
                            $=stab(Brutus,Ceasar)$
                        \end{tabular}
                        };
%\node[tlabel] {1}

\draw[dashed,->,draw=black!50] (NNP1) to[out=270,in=90] node[midway,left,text=black!50]{\footnotesize{<1>}}(SEMN1) ;
\draw[dashed,->,draw=black!50] (NNP2) to[out=270,in=90] node[midway,left,text=black!50]{\footnotesize{<1>}}(SEMN2);
\draw[dashed,->,draw=black!50] (VBD1)   to[out=270,in=90]  node[midway,left,text=black!50]{\footnotesize{<1>}} (SEMV1);
\draw[dashed,->,draw=black!50] (SEMN2)   to[out=270,in=270] node[midway,right,text=black!50]{\footnotesize{<2>}}(SEM1);
\draw[dashed,->,draw=black!50] (SEMV1)  to[out=270,in=270] node[pos=0.7,left,text=black!50]{\footnotesize{<2>}}(SEM1);
\draw[dashed,->,draw=black!50] (SEM1)  to[out=90,in=0] node[midway,right,text=black!50]{\footnotesize{<3>}}(SEM2);
\draw[dashed,->,draw=black!50] (SEMN1)  to[out=180,in=180] node[midway,left,text=black!50]{\footnotesize{<3>}}(SEM2);
%\draw[->] (mussen) to[out=0,in=90]   (M-1-8);
%\node(4)[base below of=3]{wird};
\end{tikzpicture}
\caption{Procedure of Semantics Construction} \label{fig:treesem1}
\end{figure}
\begin{enumerate}[label={ < \arabic* > }]%[ \textbf{ \arabic*  } ]
  \item
    Define the semantic representation for the verb $stab$ as $\lambda y.\lambda x.stab(x,y)$.
    Also, the semantic representations for the proper nouns Brutus and Ceasar are $Brutus$ and $Ceasar$, respectively.
  \item
    Combine the semantic representations according to the structure of this syntax tree.
    First, combine the semantic representations $\lambda y.\lambda x.stab(x,y)$ and $Ceasar$ by function application:
    $\lambda y.\lambda x.stab(x,y) @ (Ceasar)$.
    The symbol $@$ denotes function application.
    The resulted semantic representation is $\lambda x. stab(x,Ceasar)$.
  \item
    Combine the semantic representations $\lambda x.stab(x,Ceasar)$ and $Brutus$ by function application:
    $\lambda x.stab(x,Ceasar) @ (Brutus)$.
    Finally, we get the semantic representation of \eqref{eq:l1}, $stab(Brutus,Ceasar)$,
    and it is successfully constructed by this procedure.
\end{enumerate}
By this procedure, we can also construct the semantic representations introduced in \Cref{subsec:DavidSem} and \Cref{subsec:NeoDavidSem}.
%\section{Theorem Proving}


%\label{c:fsem}


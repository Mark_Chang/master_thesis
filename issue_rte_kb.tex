\subsection{Knowledge Builder}\label{subsec:issuertekb}
Since we use an extremely simple algorithm to build the logical forms of required knowledge from input logical forms,
however, in several cases, the logical forms of required knowledge can't be built by our algorithm.
We give some examples of these situations
and explain why our algorithm is unable to build them.

\subsubsection{Building Knowledge from Different Semantic Roles}
Although the thematic roles from the CKIP Parser enable the construction of Neo-Davidsonian semantic representation,
in some cases, the mismatch of thematic roles between T and H makes the Knowledge Builder unable to build the logical form of required knowledge.
For example, in \eqref{eq:rtekbs1}, the syntax trees of this example are shown in \Cref{fig:rtekbfig1}.
\begin{equation}\label{eq:rtekbs1}
\begin{aligned}
&T:\:\text{小明玩電腦遊戲} \\
&H:\:\text{小明玩電腦}
\end{aligned}
\end{equation}
Then, we use these syntax trees to build logical forms in \eqref{eq:rtekbs2}.
\begin{equation}\label{eq:rtekbs2}
\begin{array}{l l}
T:&\text{小明}(n_{0}) \wedge agent(n_{0},e) \wedge \text{玩}(e) \wedge \text{電腦}(n_{1}) \wedge property(n_{1},n_{2}) \\
  &\wedge \text{遊戲}(n_{2}) \wedge goal(n_{2},e) \\
H:&\text{小明}(n_{0}) \wedge agent(n_{0},e) \wedge \text{玩}(e) \wedge \text{電腦}(n_{1}) \wedge goal(n_{1},e) \\
\end{array}
\end{equation}
If we use the algorithm in the Knowledge Builder to construct the logical forms,
we will get the logical form $K$ in \eqref{eq:rtealgoisk3}.
\begin{equation} \label{eq:rtealgoisk3}
\begin{array}{l l}
K: \text{遊戲}(n_{2}) \wedge goal(n_{2},e) \rightarrow \text{電腦}(n_{1}) \wedge goal(n_{1},e)\\
\end{array}
\end{equation}
In fact, the word 電腦(computer) in $H$ should be aligned to the word 電腦 in $T$, 
and should not be aligned to the word 遊戲(game) in $T$.
However, the CKIP Parser will assign different roles to same word 電腦. % making the entailment $T\rightarrow H$ is false.
\begin{figure}[!h]
\begin{subfigure}[b]{0.5\textwidth}
\centering
    \begin{tikzpicture}[level distance=50pt, sibling distance=0pt]
        \Tree
        [.\node(N1){S};
         [.\node(N2){\begin{tabular}{c}agent\\NP\end{tabular}};
          [.\node(N3){\begin{tabular}{c}Head\\Nba\end{tabular}};
           \node(N4){小明};
          ]
         ]
         [.\node(N5){\begin{tabular}{c}Head\\VC2\end{tabular}};
          \node(N6){玩};
         ]
         [.\node(N7){\begin{tabular}{c}goal\\NP\end{tabular}};
          [.\node(N8){\begin{tabular}{c}property\\Nab\end{tabular}};
           \node(N9){電腦};
          ]
          [.\node(N10){\begin{tabular}{c}Head\\Nac\end{tabular}};
           \node(N11){遊戲};
          ]
         ]
        ]
    \end{tikzpicture}
    \caption{Syntax Tree of $T$}\label{fig:rtekbfig11}
\end{subfigure}
\begin{subfigure}[b]{0.5\textwidth}
\centering
    \begin{tikzpicture}[level distance=50pt, sibling distance=0pt]
        \Tree
        [.\node(N1){S};
         [.\node(N2){\begin{tabular}{c}agent\\NP\end{tabular}};
          [.\node(N3){\begin{tabular}{c}Head\\Nba\end{tabular}};
           \node(N4){小明};
          ]
         ]
         [.\node(N5){\begin{tabular}{c}Head\\VC2\end{tabular}};
          \node(N6){玩};
         ]
         [.\node(N7){\begin{tabular}{c}goal\\NP\end{tabular}};
          [.\node(N8){\begin{tabular}{c}Head\\Nab\end{tabular}};
           \node(N9){電腦};
          ]
         ]
        ]
    \end{tikzpicture}
    \caption{Syntax Tree of $H$}\label{fig:rtekbfig12}
\end{subfigure}
\caption{Syntax Trees of Example \eqref{eq:rtekbs1}}\label{fig:rtekbfig1}
\end{figure}
%Since we have not develop an algorithm to solve this problem,
%in order to match with the thematic role between $T$ and $H$, we need to manually modify the logical form.

\subsubsection{Building Knowledge from Multiple Words}
In some cases, the synonym pair between $T$ and $H$ may span to multiple words.
For example, in \eqref{eq:rtekbs3}, the synonyms between $T$ and $H$ are 電腦遊戲(computer game) and 電玩(computer game).
\begin{equation}\label{eq:rtekbs3}
\begin{aligned}
&T:\:\text{小明玩電腦遊戲} \\
&H:\:\text{小明玩電玩}
\end{aligned}
\end{equation}
However, if we use the CKIP Parser to build the syntax tree,
and convert it into logical form \eqref{eq:rtekbs4},
the word 電腦遊戲(computer game) is split into two words: 電腦(computer) and 遊戲(game).
\begin{equation}\label{eq:rtekbs4}
\begin{array}{l l}
T:&\text{小明}(n_{0}) \wedge agent(n_{0},e) \wedge \text{玩}(e) \wedge \text{電腦}(n_{1}) \wedge property(n_{1},n_{2}) \\
  &\wedge \text{遊戲}(n_{2}) \wedge goal(n_{2},e) \\
H:&\text{小明}(n_{0}) \wedge agent(n_{0},e) \wedge \text{玩}(e) \wedge \text{電玩}(n_{3}) \wedge goal(n_{3},e) \\
\end{array}
\end{equation}
If we use the algorithm in the Knowledge Builder to construct the logical form of required knowledge,
we will get the logical form $K$ in \eqref{eq:rtealgoisk5}. 
It only contains 遊戲 in $T$.
\begin{equation} \label{eq:rtealgoisk5}
\begin{array}{l l}
K:&\text{遊戲}(n_{2}) \wedge goal(n_{2},e) \rightarrow \text{電玩}(n_{3}) \wedge goal(n_{3},e)\\
\end{array}
\end{equation}
However, the logical form of required knowledge $K'$ should contain both words 電腦 and 遊戲 in $T$, 
as shown in \eqref{eq:rtealgoisk6}.
\begin{equation} \label{eq:rtealgoisk6}
\begin{array}{l l}
K':&\text{電腦}(n_{1}) \wedge property(n_{1},n_{2}) \wedge \text{遊戲}(n_{2}) \wedge goal(n_{2},e) \rightarrow \text{電玩}(n_{3}) \wedge goal(n_{3},e)\\
\end{array}
\end{equation}
%In this situation, we choose to manually create the required knowledge.

\subsubsection{Building Knowledge of Non-Action Verbs}
For some non-action verbs, they have special meaning or function in the sentence.
For example, in \eqref{eq:rtekbsx3}, this example contains the word 是(is), and it represents the state of being.
\begin{equation}\label{eq:rtekbsx3}
\begin{aligned}
&T:\:\text{學生王小明在玩電腦} \\
&H:\:\text{王小明是學生}
\end{aligned}
\end{equation}
This example can be converted into logical form in \eqref{eq:rtekbsx4}.
\begin{equation}\label{eq:rtekbsx4}
\begin{array}{l l}
T:&\text{學生}(n_{0}) \wedge apposition(n_{0},n_{1}) \wedge \text{王小明}(n_{1}) \wedge agent(n_{1},e) \wedge \text{玩}(e) \\
  &\wedge \text{電腦}(n_{2}) \wedge goal(n_{2},e)\\
H:&\text{王小明}(n_{1}) \wedge theme(n_{1},e) \wedge \text{是}(e) \wedge \text{學生}(n_{0}) \wedge range(n_{0},e)
\end{array}
\end{equation}
In this case, we require the logical form of knowledge $K$ in \eqref{eq:rtekbsx5} to solve the entailment problem.
\begin{equation}\label{eq:rtekbsx5}
\begin{array}{l l}
K:&\text{學生}(n_{0}) \wedge apposition(n_{0},n_{1}) \wedge \text{王小明}(n_{1}) \rightarrow \\
  &\text{王小明}(n_{1}) \wedge theme(n_{1},e) \wedge \text{是}(e) \wedge \text{學生}(n_{0}) \wedge range(n_{0},e)
\end{array}
\end{equation}
However, this logical form can't be easily built,
we do not intend to implement the algorithm to build these kinds of logical form in our system.
%TODO
\subsubsection{Building Knowledge of Complex Concept}
%Except for the previous two condition,
%the remaining condition that we can't build the required knowledge is categorized into this case.
%The required knowledge is a complex concept,
%and is related to several atomic formulas in both $T$ and $H$, and can't be easily built.
In some real-world cases, the structure of syntax trees can be very complex.
To build the logical form of required knowledge,
we need to consider the relationship spreading between several nodes in the syntax trees.
Since the logical form is constructed from syntax trees, the logical form can be very complex.
As example in \eqref{eq:rtekbs5}, the syntax trees of this example are shown in \Cref{fig:rtekbfig2}.
\begin{equation}\label{eq:rtekbs5}
\begin{aligned}
&T:\:\text{今年世足賽在聖保羅競技場開響}\\
&H:\:\text{今年世足賽第一場比賽在聖保羅競技場舉行}
\end{aligned}
\end{equation}
We build the logical forms of this example, as shown in \eqref{eq:rtekbs6}.
\begin{equation}\label{eq:rtekbs6}
\begin{array}{l l}
T:&\text{今年}(n_{0}) \wedge property(n_{0},n_{2}) \wedge \text{世足賽}(n_{2}) \wedge agent(n_{2},e) \wedge \text{聖保羅}(n_{1}) \\
 &\wedge property(n_{1},n_{3}) \wedge \text{競技場}(n_{3}) \wedge theme(n_{3},e) \wedge \text{開響}(e) \\
H:&\text{今年}(n_{0}) \wedge property(n_{0},n_{4}) \wedge \text{世足賽}(n_{2})  \wedge property(n_{2},n_{4})  \\
 &\wedge \text{第一場}(n_{5}) \wedge property(n_{5},n_{4}) \wedge \text{比賽}(n_{4}) \wedge agent(n_{4},e) \wedge \text{聖保羅}(n_{1}) \\
 &\wedge property(n_{1},n_{3}) \wedge \text{競技場}(n_{3}) \wedge location(n_{3},e) \wedge \text{舉行}(e) \\
\end{array}
\end{equation}
In this case, we need to add the logical form of external knowledge $K$ \eqref{eq:rtealgoisk4},
about the concept between the words 開響, 舉行(hold), 第一場(first) and 比賽(game).
\begin{equation} \label{eq:rtealgoisk4}
\begin{array}{l l}
K: \text{開響}(e) \rightarrow \text{舉行}(e) \wedge \text{第一場}(n_{5}) \wedge property(n_{5},n_{4}) \wedge \text{比賽}(n_{4}) \wedge agent(n_{4},e).
\end{array}
\end{equation}
However, the algorithm in the Knowledge Builder cannot build such complex knowledge, we need to manually build it.
Moreover, in this case, the thematic roles of the word 世足賽 are different in $H$ and in $T$,
so we have the problem of \emph{building knowledge from different semantic roles}, 
discussed in the previous section.
\begin{figure}[!h]
\begin{subfigure}[b]{\textwidth}
\centering
%\resizebox{\columnwidth}{!}{
    \begin{tikzpicture}[level distance=50pt, sibling distance=0pt]
        \Tree
        [.\node(N1){S};
         [.\node(N2){\begin{tabular}{c}agent\\NP\end{tabular}};
          [.\node(N3){\begin{tabular}{c}property\\Nd\end{tabular}};
           \node(N4){今年};
          ]
          [.\node(N5){\begin{tabular}{c}Head\\Na\end{tabular}};
           \node(N6){世足賽};
          ]
         ]
         [.\node(N7){\begin{tabular}{c}theme\\PP\end{tabular}};
          [.\node(N8){\begin{tabular}{c}Head\\P21\end{tabular}};
           \node(N9){在};
          ]
          [.\node(N10){\begin{tabular}{c}DUMMY\\NP\end{tabular}};
           [.\node(N13){\begin{tabular}{c}property\\Nba\end{tabular}};
            \node(N14){聖保羅};
           ]
           [.\node(N15){\begin{tabular}{c}Head\\Ncb\end{tabular}};
            \node(N16){競技場};
           ]
          ]
         ]
         [.\node(N17){\begin{tabular}{c}Head\\VL2\end{tabular}};
          \node(N18){開響};
         ]
        ]
    \end{tikzpicture}
%}
    \caption{Syntax Tree of $T$}\label{fig:rtekbfig21}
\end{subfigure}
\begin{subfigure}[b]{\textwidth}
\centering
\resizebox{\columnwidth}{!}{
    \begin{tikzpicture}[level distance=50pt, sibling distance=0pt]
    \Tree
    [.\node(N1){S};
     [.\node(N2){\begin{tabular}{c}agent\\NP\end{tabular}};
      [.\node(N4){\begin{tabular}{c}property\\Ndaba\end{tabular}};
       \node(N5){今年};
      ]
      [.\node(N6){\begin{tabular}{c}property\\Nac\end{tabular}};
       \node(N7){世足賽};
      ]
      [.\node(N8){\begin{tabular}{c}property\\DM\end{tabular}};
       \node(N9){第一場};
      ]
      [.\node(N10){\begin{tabular}{c}Head\\Nac\end{tabular}};
       \node(N11){比賽};
      ]
     ]
     [.\node(N12){\begin{tabular}{c}location\\PP\end{tabular}};
      [.\node(N13){\begin{tabular}{c}Head\\P21\end{tabular}};
       \node(N14){在};
      ]
      [.\node(N15){\begin{tabular}{c}DUMMY\\NP\end{tabular}};
       [.\node(N18){\begin{tabular}{c}property\\Nba\end{tabular}};
        \node(N19){聖保羅};
       ]
       [.\node(N20){\begin{tabular}{c}Head\\Ncb\end{tabular}};
        \node(N21){競技場};
       ]
      ]
     ]
     [.\node(N22){\begin{tabular}{c}Head\\VC31\end{tabular}};
      \node(N23){舉行};
     ]
    ]
    \end{tikzpicture}
}
    \caption{Syntax Tree of $H$}\label{fig:rtekbfig22}
\end{subfigure}
\caption{Syntax Trees of Example \eqref{eq:rtekbs5}}\label{fig:rtekbfig2}
\end{figure}
%\subsubsection{Redundent Word in H}

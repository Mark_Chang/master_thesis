\section{Recognizing Textual Entailment}
The Recognizing Textual Entailment (RTE) task is to recognize, given two text fragments,
whether the meaning of one text can be inferred (entailed) from the other \cite{Dagan:2005:PAS}.
Given a pair of text expressions, denoted by T, the entailing Text, and H, the entailed \emph{Hypothesis},
if the meaning of H can be inferred from the meaning of T, we say that T entails H.
For instance, in the text pair T in \eqref{eq:rtet1}, and H in \eqref{eq:rteh1},
we can infer the meaning of H in \eqref{eq:rteh1} from the meaning of T in \eqref{eq:rtet1}.
\begin{equation} \label{eq:rtet1}
T\::\:Jones\: buttered\: the\: toast\: in\: the\: bathroom\: at\: midnight.
\end{equation}
\begin{equation} \label{eq:rteh1}
H\::\:Jones\:buttered\:the\:toast.
\end{equation}
In some cases, background knowledge is required to complete the inference task in RTE.
For example, in the text pair T \eqref{eq:rtet1} and H \eqref{eq:rteh2},
if we want to infer the meaning of H in \eqref{eq:rteh2} from the meaning of T in \eqref{eq:rtet1},
we need to know the fact that \emph{at night} can be inferred from \emph{at midnight}.
\begin{equation} \label{eq:rteh2}
H\::\:Jones\: buttered\: the\: toast\: at\: night.
\end{equation}
RTE is a challenging task in natural language processing, and
the algorithms to solve RTE can be categorized into two types:
formal logic approach and machine learning approach.
\subsection{Formal Logic Approach}
Formal Logic Approach is to convert the sentences into logical forms,
and then use theorem prover to prove the truth of entailment \cite{Tatu:2006:LSA}.
For example, if we want to prove the entailment in the text pair T \eqref{eq:rtet1} and H \eqref{eq:rteh1},
we need to convert the sentences \eqref{eq:rtet1} and \eqref{eq:rteh1}
into logical forms in \eqref{eq:rtelt1} and \eqref{eq:rtelh1}, respectively.
\begin{equation} \label{eq:rtelt1}
\begin{split}
\exists e. agent(Jones,e)\wedge buttered(e)\wedge theme(toast,e) \\
\wedge location(bathroom,e) \wedge time(midnight,e).
\end{split}
\end{equation}
\begin{equation} \label{eq:rtelh1}
\exists e. agent(Jones,e)\wedge buttered(e)\wedge theme(toast,e).
\end{equation}
Then, we can use a theorem prover to prove that \eqref{eq:rtelt1} entails \eqref{eq:rtelh1} is true;
hence, H \eqref{eq:rteh1} can be inferred from T \eqref{eq:rtet1}.
To solve the cases that requires background knowledge, such as the text pair T \eqref{eq:rtet1} and H \eqref{eq:rteh2},
we can convert the sentence \eqref{eq:rteh2} into logical form \eqref{eq:rtelh2},
and the required knowledge that event happened \emph{at night} can be inferred from event happened \emph{at midnight}
can be converted into logical form \eqref{eq:rtelk1}, too.
\begin{equation} \label{eq:rtelh2}
\exists e. agent(Jones,e)\wedge buttered(e)\wedge theme(toast,e)\wedge time(night,e).
\end{equation}
\begin{equation} \label{eq:rtelk1}
\forall e. time(midnight,e) \rightarrow time(night,e)
\end{equation}
Then, the theorem prover can prove that $\eqref{eq:rtelt1}\wedge \eqref{eq:rtelk1}\rightarrow \eqref{eq:rtelh2}$ is true,
so that T \eqref{eq:rtet1} entails H \eqref{eq:rteh2} is true.

Since the logical forms need to be automatically constructed by the procedure introduced in \Cref{subsec:ComputationalSem},
first, we need to build the syntax tree from raw text.
However, using the existing parsing algorithm, such as statistical parsing, is possible to produce the erroneous syntax tree,
and the error of syntax tree usually propagates to the error of logical form.
Hence, this approach is sensitive to the error produced during the preceding stages.

\subsection{Machine Learning Approach}
The RTE task can be viewed as a binary classification problem of classifying whether T entails H is true or false.
Machine learning, mainly supervised machine learning,
including decision tree\cite{Safavian:1991:DT}, support vector machine (SVM) \cite{Cortes:1995:SVM}, and so on,
is a technique to solve this kind of classification problem.
The overall process of supervised classification is presented in \Cref{fig:superml1} \cite{NLTK:2009:SC}.
\subsubsection{a. Training Phase}
In the Training Phase, the input data consists of two parts, \emph{text} and \emph{label}, as shown in \Cref{fig:superml2}.
Text contains pairs of T and H,
and label is the truth of whether T entails H, manually annotated by human labor.
Then, the \emph{features} of text are extracted from text by \emph{feature extractor}.
These features can be anything that can be used to distinguish the difference between whether T entails H or not. 
%TODO Which is/are?
There are a wide variety of features to choose,
such as word similarity, syntactic similarity, tree edit distance, existence of negation word, difference in numerical words, etc.
The choices of features can have a huge impact on the precision of classifier.
The result after feature extraction is a matrix of features with corresponding numerical values, as shown in \Cref{fig:superml3}.
Then, the \emph{machine learning algorithm} can train a \emph{classifier model} according to the labels and the features.
\subsubsection{b. Prediction Phase}
In the Prediction Phase,
the input data consists of text, without the label of truth of entailment, as illustrated in \Cref{fig:superml4a}.
First, we use the feature extractor to extract the features from text,
similar to the matrix presented in \Cref{fig:superml3}.
Then, we can use the classifier model trained by machine learning algorithm to predict the values of labels.
Based on the values of features, the classifier model can generate the result of truth of whether T entails H, as shown in \Cref{fig:superml5}.

Unlike formal logic method, the performance of machine learning method
will not be directly affected by the error during building syntax tree;
nonetheless, supervised machine learning method requires huge amount of human labor in annotating data for training.

%Given a set of training data, then we extract the

\begin{figure}[!h]
\centering
\pgfdeclarelayer{background}
\pgfdeclarelayer{foreground}
\pgfsetlayers{background,main,foreground}


\tikzstyle{iblock}=[draw=black!50, thick,rounded corners,  %fill=gray!10,
    text centered]

\tikzstyle{jblock}=[draw, thick,  %fill=gray!10,
    text centered]

\tikzstyle{label} = [iblock, text width=3em, %fill=gray!5,
    minimum height=1em]

\tikzstyle{input} = [iblock, text width=3em, %fill=gray!5,
    minimum height=3em]

\tikzstyle{mblock} = [jblock, text width=8em, %fill=gray!20, drop shadow,
    minimum height=6em]


\tikzstyle{cblock} = [jblock, text width=8em, %fill=gray!20, drop shadow,
    minimum height=4em]

\tikzstyle{fblock} = [jblock, text width=5em, %fill=gray!20, drop shadow,
    minimum height=3em]


\begin{tikzpicture}
    \node (ma) [mblock] at (2,0) {
                                \renewcommand{\arraystretch}{1}
                                \begin{tabular}{c}
                                    machine \\ learning \\ algorithm
                                \end{tabular}
                                    };
    \node (asr1)[label] at (-6.,1){label};
    \node (asr2)[input] at (-6,-0.5) {text };
    \node (fa) [fblock] at (-3.5,-0.5) {
                                    \renewcommand{\arraystretch}{1}
                                    \begin{tabular}{c}
                                        feature \\ extractor
                                    \end{tabular}
                                        };
    \node (asr3)[input] at (-1,-0.5) {features};
    \path (asr1.north)+(0,0.5) node (asrs) {(a) Training};

    \path [draw, ->] (asr1.east) -- (ma.152) ;
    \path [draw, ->] (asr2.east) -- (fa.west);
    \path [draw, ->] (fa.east) -- (asr3.west);
    \path [draw, ->] (asr3.east) -- (ma.195);

    \begin{pgfonlayer}{background}
        \path (asrs.west |- asrs.north)+(-0.5,0.3) node (a) {};
        \path (ma.south -| ma.east)+(+2.3,-0.3) node (b) {};
        \path[rounded corners, draw=black!50, dashed, %fill=gray!30
             ]
            (a) rectangle (b);
    \end{pgfonlayer}

    \node (mb) [cblock] at (2,-4) {
                                \renewcommand{\arraystretch}{1}
                                \begin{tabular}{c}
                                    classifier \\ model
                                \end{tabular}
                                    };
    \node (asr5)[input] at (-6,-4) {text};
    \node (fb) [fblock] at (-3.5,-4) {
                                    \renewcommand{\arraystretch}{1}
                                    \begin{tabular}{c}
                                        feature \\ extractor
                                    \end{tabular}
                                        };
    \node (asr6)[input] at (-1,-4) {features};
    \node (asr4)[label] at (5,-4){label};

    \path (asr5.north)+(0,0.5) node (asrs2) {(b) Prediction};
    \path [draw, ->] (mb.east) -- (asr4.west) ;
    \path [draw, ->] (asr5.east) -- (fb.west);
    \path [draw, ->] (fb.east) -- (asr6.west);
    \path [draw, ->] (asr6.east) -- (mb.west);

    \begin{pgfonlayer}{background}
        \path (asrs2.west |- asrs2.north)+(-0.5,0.3) node (c) {};
        \path (mb.south -| mb.east)+(+2.4,-0.4) node (d) {};
        \path[rounded corners, draw=black!50, dashed, %fill=gray!30
             ]
            (c) rectangle (d);
    \end{pgfonlayer}

    \path [draw, ->] (ma.south) -- (mb.north);

\end{tikzpicture}
\caption{Procedure of Supervised Machine Learning for Classification} \label{fig:superml1}
\end{figure}



\begin{figure}[!h]
\centering
\begin{subfigure}[b]{\columnwidth}
\begin{tabular}{c|l c}
  \textbf{id}& \textbf{text} & \textbf{label} \\\hline
  1 & \begin{tabular}{l}
      T : An American won the Nobel prize for literature. \\
      H : An American won the Nobel prize.
      \end{tabular}    & True \\ \hline
  2 & \begin{tabular}{l}
      T: The Council of Europe has 45 member states.  \\
      H: The Council of Europe is made up by 45 member states.
      \end{tabular}    & True \\ \hline
  3 & \begin{tabular}{l}
      T: Jody saw Gary sign the contract.\\
      H: Judy saw Gary sign the contract and his secretary make a copy.
      \end{tabular}    & False \\ \hline
  ... & ........    & ...  \\
\end{tabular}
\caption{Input Data in Training Phase} \label{fig:superml2}
\end{subfigure}

\begin{subfigure}[b]{\columnwidth}
\centering
\begin{tabular}{c|l c}
  \textbf{id}   & \textbf{text}  \\\hline
  1 & \begin{tabular}{l}
      T : Einstein won the Nobel prize for physics. \\
      H : Einstein won the Nobel prize.
      \end{tabular}     \\ \hline
  2 & \begin{tabular}{l}
      T: The Council of Europe has 45 member states.  \\
      H: The Council of Europe is made up by 42 member states.
      \end{tabular}    \\ \hline
  3 & \begin{tabular}{l}
      T: Nancy saw Steven playing the piano in the living room.\\
      H: Nancy saw Steven playing the piano.
      \end{tabular}    \\ \hline
  ... & ........      \\
\end{tabular}
\caption{Input Data in Prediction Phase} \label{fig:superml4a}
\end{subfigure}
\caption{Input Data for Machine Learning} %\label{fig:superml4}
\end{figure}

\begin{figure}[!h]
\begin{subfigure}[b]{0.5\textwidth}
\centering
        \begin{tabular}{c|c c c c }
          \emph{id}   &  $feature_{1}$ & $feature_{2}$ & $feature_{3}$&... \\\hline
            1 & 2.0000  & 3.5001  & 1.3319 & ... \\
            2 & -3.0000 & 2.4232  & 2.1121 & ... \\
            3 & -6.0000 & -1.7121 & 1.0023 &...  \\
          ... & ...     & ...     & ...    & ...\\
        \end{tabular}
        \caption{Matrix of Feature Values} \label{fig:superml3}
\end{subfigure}
\begin{subfigure}[b]{0.5\textwidth}
\centering
    \begin{tabular}{c|c }
      \emph{id}   &  \emph{label}  \\\hline
        1 & True  \\
        2 & False \\
        3 & True  \\
      ... & ...   \\
    \end{tabular}
    \caption{Result of Prediction} \label{fig:superml5}
\end{subfigure}
\caption{Feature Values and Results} \label{fig:superml9}
\end{figure}
%
%\begin{figure}[H]
%\begin{subfigure}[b]{.5\textwidth}
%\end{subfigure}
%\begin{subfigure}[b]{.5\textwidth}
%\end{subfigure}
%\end{figure}
%

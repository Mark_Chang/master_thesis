\section{Automated Theorem Proving}
Automated Theorem Proving (ATP) is the study of how computer programs can prove that whether a logical statement is true or false.
One of the methods for ATP is \emph{refutation}.
This method is to negate the logical statement supposed to be proved (the conclusion),
and then add it to the list of premises.
For example, given two logical formulas in \eqref{eq:atp1}, the premise is $P$ and the conclusion is $Q$.
\begin{equation}\label{eq:atp1}
\begin{aligned}
&P = A(x_{1}) \wedge B(x_{2}) \wedge C(x_{3}) \\
&Q = A(x_{1}) \wedge B(x_{2})
\end{aligned}
\end{equation}
If we want to prove that $P \rightarrow Q$ is true,
we can use a theorem prover to prove whether $P \wedge \neg Q$, \eqref{eq:atp2}, is satisfiable or not.
\begin{equation}\label{eq:atp2}
\begin{aligned}
&P \wedge \neg Q \\
&= A(x_{1}) \wedge B(x_{2}) \wedge C(x_{3}) \wedge \neg( A(x_{1}) \wedge B(x_{2}) ) \\
&= A(x_{1}) \wedge B(x_{2}) \wedge C(x_{3}) \wedge ( \neg A(x_{1}) \vee \neg B(x_{2}) ) \\
\end{aligned}
\end{equation}
If \eqref{eq:atp2} is unsatisfiable, $P\rightarrow Q$ is true; otherwise, $P\rightarrow Q$ is false.
\emph{Tableaux} and \emph{Resolution} are two main algorithms that can solve the problem of ATP.

\subsection{Tableaux}\label{subsec:prelialgo1}
The goal of tableaux algorithm is to show that whether a formula is satisfiable or not.
The main idea of tableaux algorithm is to break down a complex formula into several atomic formulas,
until complementary pairs of atomic formulas are produced or no further possible break-down exists.
This method works on a tree (tableau) with nodes labeled with formulas.
At each step, we modify the tree by adding a descendant to a leaf in the tree.
For example, if we want to prove \eqref{eq:atp2} is unsatisfiable,
we initialize the root of tree as $P \wedge \neg Q$. %TODO
Then, we decompose the formula at the root of tree and check the satisfiability by the following procedures,
shown in \Cref{fig:prelitabf1}.
\begin{figure}[p]
\centering
\pgfdeclarelayer{background}
\pgfdeclarelayer{foreground}
\pgfsetlayers{background,main,foreground}
\tikzstyle{node} = [ text centered]
\tikzstyle{resource} = [ text centered]
\tikzstyle{system} = [ text centered]
\tikzstyle{flow} = [draw=black, thick,looseness=0]
\tikzstyle{flow2} = [draw=black, thick,dashed]
\tikzstyle{ftext} = [midway,text=black!50]

\begin{subfigure}[b]{\columnwidth}
%\resizebox{0.5\columnwidth}{!}{
\begin{tikzpicture}
\node(TB1)  [node                           ] at (0,0){$A(x_{1}) \wedge B(x_{2}) \wedge C(x_{3}) \wedge ( \neg A(x_{1}) \vee \neg B(x_{2}) )$};
\node(TB2)  [node,      below =0.7cm of TB1  ]         {$A(x_{1})$};
\node(TB3)  [node,      below =0.7cm of TB2  ]         {$B(x_{2}) \wedge C(x_{3}) \wedge ( \neg A(x_{1}) \vee \neg B(x_{2}) )$};
\node(TB4)  [node,      below =0.7cm of TB3  ]         {$B(x_{2})$};
\node(TB5)  [node,      below =0.7cm of TB4  ]         {$C(x_{3}) \wedge ( \neg A(x_{1}) \vee \neg B(x_{2}) )$};
\node(TB6)  [node,      below =0.7cm of TB5  ]         {$C(x_{3})$};
\node(TB7)  [node,      below =0.7cm of TB6  ]         {$\neg A(x_{1}) \vee \neg B(x_{2})$};
\node(TB81) [node, below left =  1cm and -0.7cm of TB7  ]         {$\neg A(x_{1})$};
\node(TB82) [node, below right=  1cm and -0.7cm of TB7  ]         {$\neg B(x_{2})$};

\draw[flow]  (TB1) to[out=270 ,in=90  ] node[ftext,left ]{\footnotesize{<1>}} (TB2) ;
\draw[flow]  (TB2) to[out=270 ,in=90  ] node[ftext,left ]{\footnotesize{<1>}} (TB3) ;
\draw[flow]  (TB3) to[out=270 ,in=90  ] node[ftext,left ]{\footnotesize{<1>}} (TB4) ;
\draw[flow]  (TB4) to[out=270 ,in=90  ] node[ftext,left ]{\footnotesize{<1>}} (TB5) ;
\draw[flow]  (TB5) to[out=270 ,in=90  ] node[ftext,left ]{\footnotesize{<1>}} (TB6) ;
\draw[flow]  (TB6) to[out=270 ,in=90  ] node[ftext,left ]{\footnotesize{<1>}} (TB7) ;
\draw[flow]  (TB7) to[out=270 ,in=90  ] node[ftext,above]{\footnotesize{<2>}} (TB81) ;
\draw[flow]  (TB7) to[out=270 ,in=90  ] node[ftext,above]{\footnotesize{<2>}} (TB82) ;
\draw [flow2] (TB2) to[out=180,in=180,looseness = 1.3 ] node[ftext,left ]{\footnotesize{<3>}} (TB81) ;
\draw [flow2] (TB4) to[out=0,in=0,    looseness = 1.3 ] node[ftext,right]{\footnotesize{<3>}} (TB82) ;
\end{tikzpicture}
%}
\caption{$P \wedge \neg Q$ is Unsatisfiable } \label{fig:prelitabf1}
\end{subfigure}

\begin{minipage}[t][0.5cm][t]{\textwidth}
\end{minipage}

\begin{subfigure}[b]{\columnwidth}
\begin{tikzpicture}
\node(TB1)  [node                           ] at (0,0){$A(x_{1}) \wedge B(x_{2}) \wedge ( \neg A(x_{1}) \vee \neg B(x_{2}) \vee \neg C(x_{3}))$};
\node(TB2)  [node,      below =0.7cm of TB1  ]         {$A(x_{1})$};
\node(TB3)  [node,      below =0.7cm of TB2  ]         {$B(x_{2}) \wedge ( \neg A(x_{1}) \vee \neg B(x_{2}) \vee \neg C(x_{3}))$};
\node(TB4)  [node,      below =0.7cm of TB3  ]         {$B(x_{2})$};
\node(TB5)  [node,      below =0.7cm of TB4  ]         {$\neg A(x_{1}) \vee \neg B(x_{2}) \vee \neg C(x_{3}))$};
\node(TB61) [node, below left =1cm and 0cm of TB5  ]         {$\neg A(x_{1})$};
\node(TB62) [node,      below =1cm of  TB5           ]         {$\neg B(x_{2})$};
\node(TB63) [node, below right=1cm and 0cm of TB5  ]         {$\neg C(x_{3})$};
\node(TT1)  [left =1cm of TB2] {};
\node(TT2)  [right =1.6cm of TB4] {};

\draw[flow]  (TB1) to[out=270 ,in=90  ] node[ftext,left ]{\footnotesize{<1>}}(TB2) ;
\draw[flow]  (TB2) to[out=270 ,in=90  ] node[ftext,left ]{\footnotesize{<1>}}(TB3) ;
\draw[flow]  (TB3) to[out=270 ,in=90  ] node[ftext,left ]{\footnotesize{<1>}}(TB4) ;
\draw[flow]  (TB4) to[out=270 ,in=90  ] node[ftext,left ]{\footnotesize{<1>}}(TB5) ;
\draw[flow]  (TB5) to[out=270 ,in=90  ] node[ftext,above]{\footnotesize{<2>}}(TB61) ;
\draw[flow]  (TB5) to[out=270 ,in=90  ] node[ftext,left ]{\footnotesize{<2>}}(TB62) ;
\draw[flow]  (TB5) to[out=270 ,in=90  ] node[ftext,above]{\footnotesize{<2>}}(TB63) ;
\draw [flow2] (TB2) to[out=180,in=0, looseness = 1.5] (TT1) to[out=180,in=180, looseness = 1]
                    node[ftext,left ]{\footnotesize{<3>}}(TB61) ;
\draw [flow2] (TB4) to[out=0,in=180](TT2) to[out=0,in=270,looseness = 1.4 ]
                    node[ftext,right]{\footnotesize{<3>}}(TB62) ;

\end{tikzpicture}
\caption{$P \wedge \neg Q$ is Satisfiable } \label{fig:prelitabf2}
\end{subfigure}
\caption{Procedure of Tableaux Algorithm}
\end{figure}
\begin{enumerate}[label={ < \arabic* > }]
\item For every conjunction of formula, such as $A \wedge B$,
we decompose it into two formulas, $A$ and $B$,
and append $A$ to the node with formula $A \wedge B$ first, and then append $B$ to the node with formula $A$.
In \Cref{fig:prelitabf1},
we decompose the formula in the root, $A(x_{1}) \wedge B(x_{2}) \wedge C(x_{3}) \wedge ( \neg A(x_{1}) \vee \neg B(x_{2}) )$,
into $A(x_{1})$ and $B(x_{2}) \wedge C(x_{3}) \wedge ( \neg A(x_{1}) \vee \neg B(x_{2}) )$.
We append $A(x_{1})$ to the root of the tree first,
and then append $B(x_{2}) \wedge C(x_{3}) \wedge ( \neg A(x_{1}) \vee \neg B(x_{2}) )$ to the node with formula $A(x_{1})$.
Since $B(x_{2}) \wedge C(x_{3}) \wedge ( \neg A(x_{1}) \vee \neg B(x_{2}) )$
can be further decompose into $B(x_{2})$ and $C(x_{3}) \wedge ( \neg A(x_{1}) \vee \neg B(x_{2}) )$,
we decompose it and do the same procedure again, until no conjunction can be decomposed.
\item For every disjunction of formula, such as $A \vee B$,
we decompose it into two formulas, $A$ and $B$, and then append both of them to the node with formula $A \vee B$.
In \Cref{fig:prelitabf1},
we decompose the formula $\neg A(x_{1}) \vee \neg B(x_{2})$ into $\neg A(x_{1})$ and $\neg B(x_{2})$,
and then append both of them to the node with formula $\neg A(x_{1}) \vee \neg B(x_{2})$.
\item If there is no further possible decomposition, we check the satisfiability of this tableau by the following procedure.
For every path from the root to the leaf of this tableau,
if a path contains both $A$ and $\neg A$, and $A$ is an atomic formula, this path is \emph{closed}.
If all the paths in the tableau are closed, this tableau is unsatisfiable; otherwise, it is satisfiable.
In \Cref{fig:prelitabf1}, the path from the root to the node with formula $\neg A(x_{1})$ is closed,
because it contains both $\neg A(x_{1})$ and $A(x_{1})$, annotated by the dashed line.
Also, the path from the root to the node with formula $\neg B(x_{2})$ is closed for the same reason.
Since all the paths in this tableau are closed, this tableau is unsatisfiable.
If we exchange $P$ and $Q$, as shown in \Cref{fig:prelitabf2},
the path from the root to the node with formula $\neg C(x_{3})$ is not closed,
because it does not contain both $\neg C(x_{3})$ and $C(x_{3})$. %TODO do not or did not
Since this tableau contains a non-closed branch, it is satisfiable.
\end{enumerate}

\subsection{Resolution}\label{subsec:prelialgo2}
Resolution is an inference technique, 
and it produces a new clause by combining two clauses containing complementary atomic formulas.
For example, given two clauses, $A(x) \vee B(x)$ and $\neg A(x)$,
we can derive the conclusion $B(x)$ by the resolution rule shown in \eqref{eq:res1}.
\begin{equation}\label{eq:res1}
\begin{array}{c}
A(x) \vee B(x) , \neg A(x)\\\hline
B(x)
\end{array}
\end{equation}
We can use the resolution rule to prove that \eqref{eq:atp2} is unsatisfiable, and the procedure is presented in \Cref{fig:preliresf1}.
\begin{figure}[!h]
\centering
\begin{subfigure}[b]{\columnwidth}
\centering
\begin{tabular}{l l l}
1.& \{$A(x_{1})$\}                  & \\
2.& \{$B(x_{2})$\}                  & \\
3.& \{$C(x_{3})$\}                  & \\
%4.& \{D(x_{4})\}                  & \\
%4.& \{\neg A(x_{1}), \neg B(x_{2}), \neg C(x_{3})\} & \\
4.& \{$\neg A(x_{1}), \neg B(x_{2})$\}         & \\%resolve(1, 5) \\
5.& \{$\neg B(x_{2})$\}                 & resolve(1, 4) \\
6.& \{\}                       & resolve(2, 5) \\
\end{tabular}
\caption{$P \wedge \neg Q$ is Unsatisfiable }\label{fig:preliresf1}
\end{subfigure}

\begin{minipage}[t][1cm][t]{\textwidth}
\end{minipage}

\begin{subfigure}[b]{\columnwidth}
\centering
\begin{tabular}{l l l}
1.& \{$A(x_{1})$\}                          & \\
2.& \{$B(x_{2})$\}                          & \\
%3.& \{C(x_{3})\}                          & \\
%4.& \{\neg A(x_{1}), \neg B(x_{2}), \neg C(x_{3}), -D(x_{4})\} & \\
3.& \{$\neg A(x_{1}), \neg B(x_{2}), \neg C(x_{3})$\}         & \\%resolve(1, 4)\\
4.& \{$\neg B(x_{2}), \neg C(x_{3})$\}                 & resolve(1, 3)\\
5.& \{$\neg C(x_{3})$\}                         & resolve(2, 4)\\
\end{tabular}
\caption{$P \wedge \neg Q$ is Satisfiable }\label{fig:preliresf2}
\end{subfigure}
\caption{Procedure of Resolution Algorithm}
\end{figure}
\begin{itemize}
\item[In] 1-3, since $P$ is conjunctions of atomic formulas, we decompose $P$ into three formulas.
\item[In] 4, since $\neg Q$ is disjunctions of atomic formulas, we do not need to decompose it.
\item[In] 5, we use the rule of resolution with 1 and 4 to eliminate the atomic formula $\neg A(x_{1})$ in 4.
\item[In] 6, we eliminate $\neg B(x_{2})$ in 5 by resolution, and we get an empty clause.
\end{itemize}
After these procedures, if an empty clause can be generated, it means that $P \wedge \neg Q$ is unsatisfiable.
On the other hand, if we exchange $P$ and $Q$, as illustrated in \Cref{fig:preliresf2},
we will leave a clause $\neg C(x_{3})$ at the end of this procedure.
The clause $\neg C(x_{3})$ can't be eliminated by the rule of resolution.
As a result, an empty clause can't be derived; hence $P \wedge \neg Q$ is satisfiable.

